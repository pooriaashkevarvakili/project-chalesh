import Axios from "axios"
const posts = {
    namespaced: true,
    state: {
        posts: []
    },
    getters: {
        getPosts(state: any) {
            return state.posts
        }
    },
    mutations: {
        getPost(state: any, posts: any) {
            state.posts = posts
        }
    },
    actions: {
        async getPost({ commit }) {
            try {
                await Axios.get('https://jsonplaceholder.typicode.com/todos').then((res) => {
                    commit('getPost', res.data)
                })
            } catch (error) {
                alert(error)
            }
        }
    }
}

export default posts